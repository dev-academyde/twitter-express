import { Request, Response, NextFunction } from 'express';

export const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  if (req.headers.authorization && req.headers.authorization === 'secret') {
    next();
  } else {
    res.send('Not allowed');
  }
};
