export type Tweet = {
  id: number;
  author: string;
  body: string;
};

export interface IDatabase {
  findById(id: number): Tweet | null;
  updateById(id: number, update: Partial<Tweet>): Tweet;
  find(): Tweet[];
  create(author: string, body: string): Tweet;
}
