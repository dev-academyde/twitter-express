import { IDatabase, Tweet } from '.';

const tweets: Tweet[] = [];

export class MemoryDatabase implements IDatabase {
  findById(id: number) {
    return tweets.find(tweet => tweet.id === id) || null;
  }

  updateById(id: number, update: Partial<Tweet>) {
    const index = tweets.findIndex(tweet => tweet.id === id);

    if (index === -1) {
      throw new Error(`Tweet with id ${id} not found`);
    }

    const updatedTweet = {
      ...tweets[index],
      ...update,
    };

    tweets[index] = updatedTweet;

    return updatedTweet;
  }

  find() {
    return tweets;
  }

  create(author: string, body: string) {
    const tweet = {
      id: tweets.length + 1,
      body,
      author,
    };

    tweets.push(tweet);

    return tweet;
  }
}
