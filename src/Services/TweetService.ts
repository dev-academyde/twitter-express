import { IDatabase, Tweet } from '../Database';

export class TweetService {
  constructor(private readonly database: IDatabase) {}

  getTweet(id: number) {
    return this.database.findById(id);
  }

  updateTweet(id: number, update: Partial<Tweet>) {
    return this.database.updateById(id, update);
  }

  getTweets() {
    return this.database.find();
  }

  createTweet(author: string, body: string) {
    return this.database.create(author, body);
  }
}
