import { TweetService } from '../TweetService';
import { IDatabase, Tweet } from '../../Database';

class MockDB implements IDatabase {
  findById(id: number) {
    return {
      id: 1,
      author: 'Foo',
      body: 'salami',
    };
  }

  updateById(id: number, update: Partial<Tweet>) {
    return {
      id: 1,
      author: 'Foo',
      body: 'salami',
    };
  }

  find() {
    return [
      {
        id: 1,
        author: 'Foo',
        body: 'salami',
      },
      {
        id: 3,
        author: 'Bar',
        body: 'avocado',
      },
    ];
  }

  create(author: string, body: string) {
    return {
      id: 1,
      author: 'Foo',
      body: 'salami',
    };
  }
}

describe('TweetService', () => {
  it('should add new tweet', () => {
    const ts = new TweetService(new MockDB());

    const tweet = ts.createTweet('foo', 'bar');

    expect(tweet.id).toBeTruthy();
  });

  it('should return all tweets', () => {
    const ts = new TweetService(new MockDB());

    expect(ts.getTweets()).toMatchSnapshot();
  });
});
