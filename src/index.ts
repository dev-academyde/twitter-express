import express from 'express';
import { TweetService } from './Services/TweetService';
import { MemoryDatabase } from './Database/MemoryDatabase';
import bodyParser from 'body-parser';
import { authMiddleware } from './Middlewares/auth';

const app = express();
const tweetService = new TweetService(new MemoryDatabase());

app.use(bodyParser.json());

app.get('/tweets', (req, res) => {
  res.send(tweetService.getTweets());
});

app.get('/tweet/:id', (req, res) => {
  res.send(tweetService.getTweet(Number(req.params.id)));
});

app.post('/tweets', authMiddleware, (req, res) => {
  console.log(req.body);

  const { author, body } = req.body;

  res.send(tweetService.createTweet(author, body));
});

app.put('/tweet/:id', (req, res) => {
  res.send(tweetService.updateTweet(Number(req.params.id), req.body));
});

app.listen(8080, () => console.log('Server started'));
